<?php
/**
 * Password Decipher class that utilizes SHA1 to create secure passwords with salt.
 * 
 * @author		Earl Peter G <earl.gangoso@outlook.com>
 * @copyright	2015-2018
 * @license		https://opensource.org/licenses/MIT MIT
 * @package		php-password-decipher
 * @version		0.1.0
 */
class PasswordDecipher {
	/**
	 * Checks if a plain password matches the hashed password with salting.
	 * @param string $hashedpassword SHA1-Hashed of password + salt
	 * @param string $plainpassword Plain password to be checked
	 * @return bool
	 */
	public static function isValid($hashedpassword, $plainpassword) {
		if (strlen($hashedpassword) >= 41) {
			// SHA1 has 40 characters long
			$hashed = substr($hashedpassword, 0, 40);
			$salt = substr($hashedpassword, 41);
			$hashedwithsalt = sha1($plainpassword . $salt) . ':' . $salt;

			return $hashedwithsalt === $hashedpassword;
		}

		// invalid password format
		return false;
	}

	/**
	 * Generates a secure password by creating a random salt and using SHA1.
	 * @param string $plainpassword
	 * @return string
	 */
	public static function create($plainpassword) {
		$salt = self::generateSalt();
		$hashed = sha1($plainpassword . $salt);

		return $hashed . ':' . $salt;
	}

	/**
	 * Generates a 16-length random salt.
	 * @return $string
	 */
	private static function generateSalt() {
        $alphabet = "abcdef0123456789";
        $key = "";
       
        for($i = 0; $i < 16; $i++) {
            $offset = rand(0, strlen($alphabet) - 1);
            $key .= $alphabet[$offset];
        }
               
        return $key;
    }
}