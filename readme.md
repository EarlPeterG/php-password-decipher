# Password Decipher Class

## Installation

To utilize this class, require PasswordDecipher.php into your project.

    require_once ('PasswordDecipher.php');

## Installation with composer

You can also install the library with composer.

    composer require earlpeterg/php-password-decipher:0.1.0

## Usage

There are two functions available: `isValid($hashedpassword, $plainpassword)` and `create($plainpassword)`.

`create` generates a secure password by creating a random salt and using SHA1. Example:

    $plainpassword = 'qwerty1234';
    $hashedpassword = PasswordDecipher::create($plainpassword);
    // may return '45a2c4a6c722b28ad88510197173aac47d9a3bcd:b0e02940ca020f45'
    // where 'b0e02940ca020f45' is the random salt generated during runtime.

`isValid` checks if a plain password matches the hashed password with salting. Example:

    $hashedpassword = '45a2c4a6c722b28ad88510197173aac47d9a3bcd:b0e02940ca020f45';
    $plainpassword = 'qwerty1234';

    $isvalid = PasswordDecipher::isValid($hashedpassword, $plainpassword);
    // returns true

    $hashedpassword = '45a2c4a6c722b28ad88510197173aac47d9a3bcd:b0e02940ca020f45';
    $plainpassword = 'invalidpassword';

    $isvalid = PasswordDecipher::isValid($hashedpassword, $plainpassword);
    // returns false

## How the password is generated.

 1. A salt is generated from random 16-digit hexadecimal value.
 2. Password is created by calculating the SHA1 hash of password + salt.

## How the password is checked.

 1. Get the salt from the end of the salted-hashed output.
 2. Password is created by calculating the SHA1 hash of plain password + salt.
 3. Compare old password and new password if it matches.